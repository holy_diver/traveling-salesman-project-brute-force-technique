#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct cities
{
    int left[4];
};

void bruteForceTSP(int costMatrix[][4], int startCity, int visitedCities[]);
void divideAndConquer(int costMatrix[][4], int currentCity, int visitedCities[], struct cities citiesLeft, int citiesLeftCount, int cost);
void printArray(int array[], int length);
int lowestCost = INT_MAX;
int lowestCostCities[4];
int solutionUsed = 0;

int main()
{
    int visitedCities[4];
    int costMatrix[4][4] = //answer = 7
        {{0, 4, 1, 3},
         {4, 0, 2, 1},
         {1, 2, 0, 5},
         {3, 1, 5, 0}};
    /*
    int costMatrix[4][4] =  //answer = 80
        {{0, 10, 15, 20},
         {10, 0, 35, 25},
         {15, 35, 0, 30},
         {20, 25, 30, 0}};
*/
    int startCity = 0;
    while ((startCity < 1) || (startCity > 4))
    {
        printf("Enter the number of the city to start the calculation with. (Between 1 and 4)\n");
        scanf("%d", &startCity);
    }
    startCity -= 1;
    int i;
    for (i = 0; i < 4; i++)
    {
        visitedCities[i] = -1;
    }

    bruteForceTSP(costMatrix, startCity, visitedCities);

    printf("shortest road possible length: %d\n", lowestCost);
    printArray(lowestCostCities, 4);

    printf("%d\n", startCity + 1);
    printf("solution used = %d", solutionUsed);
}

void bruteForceTSP(int costMatrix[][4], int startCity, int visitedCities[])
{
    struct cities citiesLeft = {{0, 1, 2, 3}};
    int cost = 0;
    divideAndConquer(costMatrix, startCity, visitedCities, citiesLeft, 3, cost);
}

void divideAndConquer(int costMatrix[][4], int currentCity, int visitedCities[], struct cities citiesLeft, int citiesLeftCount, int cost)
{
    if (citiesLeftCount == 0)
    {
        int i;
        for (i = 0; i < 4; i++)
        {
            if (citiesLeft.left[i] != -1)
            {
                break;
            }
        }
        int lastCity = citiesLeft.left[i];
        visitedCities[3] = lastCity;
        cost += costMatrix[currentCity][lastCity] + costMatrix[lastCity][visitedCities[0]]; //road to last city left + to the beginning city

        printArray(visitedCities, 4);
        printf("%d ---------> cost = %d\n", visitedCities[0] + 1, cost);

        //if cost is lower than last lowest, copy this array to the last lowest
        if (cost < lowestCost)
        {
            lowestCost = cost;
            for (i = 0; i < 4; i++)
            {
                lowestCostCities[i] = visitedCities[i];
            }
            solutionUsed++;
        }

        return;
    }

    int i;
    citiesLeft.left[currentCity] = -1;
    for (i = 0; i < 4; i++)
    {
        if (citiesLeft.left[i] == -1)
            continue;
        visitedCities[3 - citiesLeftCount] = currentCity;
        int nextCity = citiesLeft.left[i];
        divideAndConquer(costMatrix, nextCity, visitedCities, citiesLeft, citiesLeftCount - 1, cost + costMatrix[currentCity][nextCity]);
    }
}

void printArray(int array[], int length)
{
    int i;
    for (i = 0; i < length; i++)
    {
        printf("%d->", array[i] + 1);
    }
}